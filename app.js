var express = require('express');
var path = require('path');
var debug = require('debug')('ui5ci:server');


var app = express();

app.use(express.static(__dirname + '/public'));


app.post('/', function (req, res) {
    console.log("post!");
    console.log(req.body);
    res.send("ok");
    debug(req);
});


module.exports = app;
